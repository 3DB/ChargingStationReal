"""
Handles the communication with the charging server. Tells the charging control to begin charging.

:author: Thogs
:date: 21.7.18 (Jule's B-Day)
"""

import json
import pygame
import qrcode
import logging
from time import sleep, time
import requests
from threading import Thread
from random import randint
from os import system

from flask import abort, request, jsonify, Flask
from flask_classy import FlaskView
from flask_classy import route
from RPi import GPIO

logger = logging.getLogger("ChargingStation")

AUTHORISED_SIGNAL_PIN = 21
IS_CHARGING_PIN = 20
RAND_ID_SHOW_TIME = 120
DEFAULT_TEXT = "Scan the QR-Code to begin charging..."
CHARGING_TEXT = "Charging..."
PLUG_IN_TEXT = "Plug in to charge..."
STATION_ID = 42
CHARGING_SERVER = "http://Tardis:5003"
QR_PATH = "/tmp/qr.png"
LEN_RAND_ID = 10
DEBUG_SCALE_SECS_TO_MIN = True
CHARGE_POWER = 42 * 2       # 42V * 2A
HEARTBEAT_TIME = 5 * 60     # 5 min

CUR_RAND_ID = None

GPIO.setmode(GPIO.BCM)
GPIO.setup(AUTHORISED_SIGNAL_PIN, GPIO.OUT)
GPIO.output(AUTHORISED_SIGNAL_PIN, GPIO.LOW)

GPIO.setup(IS_CHARGING_PIN, GPIO.IN)

class Colour:
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    GREEN = (0, 255, 0)
    RED = (255, 0, 0)
    BLUE = (0, 0, 255)

BG_COL = Colour.WHITE
FG_COL = Colour.BLACK

class ChargingStation(FlaskView):
    route_base = '/'
    currently_waiting = False
    cur_rand_timeout_threads = {}

    def __init__(self):
        # DynDNS
        # TODO: Add DynDNS

        pygame.init()
        pygame.font.init()
        self.screen = pygame.display.set_mode((1184, 624), pygame.FULLSCREEN)

        # Logo
        self.logo_img = pygame.image.load("AncharLogo.png")
        self.logo_img = pygame.transform.scale(self.logo_img, (530, 450))

        # Texts
        self.font = pygame.font.SysFont("Ubuntu", 85)
        self.id_font = pygame.font.SysFont("Ubuntu", 200)
        self.font.set_italic(True)
        self.font.set_bold(True)
        self.id_font.set_italic(True)
        self.id_font.set_bold(True)

        self.surf_default_text = self.font.render(DEFAULT_TEXT, False, FG_COL)
        self.surf_charging_text = self.font.render(CHARGING_TEXT, False, FG_COL)
        self.surf_plug_in_text = self.font.render(PLUG_IN_TEXT, False, FG_COL)

        self.show_main_screen()

    def gen_qr_code(self):
        global CUR_RAND_ID
        CUR_RAND_ID = ''.join(str(randint(0,9)) for _ in range(LEN_RAND_ID))
        qr_data = {"url": CHARGING_SERVER[7:], "stationID": STATION_ID, "rand_id": CUR_RAND_ID}
        qr_data = json.dumps(qr_data)

        qr = qrcode.QRCode()
        qr.add_data(qr_data)
        qr.make(fit=True)
        img = qr.make_image(fill_color="black", back_color="white")
        img.save(QR_PATH)

        self.qr_img = pygame.image.load(QR_PATH)
        self.qr_img = pygame.transform.scale(self.qr_img, (650, 550))

    def show_text(self, text, xPos=None, yPos=None):
        if xPos == None:
            xPos = self.screen.get_width() / 2 - text.get_width() / 2

        if yPos == None:
            yPos = self.screen.get_height() / 2 - text.get_height() / 2

        self.screen.blit(text, (xPos, yPos))
        pygame.display.update()

    def show_main_screen(self):
        self.screen.fill((BG_COL))
        self.gen_qr_code()
        self.screen.blit(self.qr_img, self.get_logo_pos(self.qr_img))
        self.show_text(self.surf_default_text, yPos=33)
        pygame.display.update()

    def show_charging_screen(self, bPluggedIn):
        self.screen.fill((BG_COL))
        self.screen.blit(self.logo_img, self.get_logo_pos(self.logo_img))
        text = self.surf_charging_text if bPluggedIn else self.surf_plug_in_text
        self.show_text(text, yPos=33)
        pygame.display.update()

    def get_logo_pos(self, surf):
        return (self.screen.get_width() / 2 - surf.get_width() / 2, self.screen.get_height() / 2 - surf.get_height() / 2 + 40)

    """
    @route("/show_rand_id", methods=["POST", "GET"])
    def show_rand_id(self):
        values = request.get_json()
        if not "rand_id" in values:
            abort(400)

        if self.currently_waiting:
            abort(503)

        self.screen.fill((BG_COL))
        self.show_text(self.id_font.render("Your ID: " + values["rand_id"], False, FG_COL))

        self.currently_waiting = True
        Thread(target=self._rand_id_timeout).start()

        return jsonify({"success" : True})
    """

    """
    def _rand_id_timeout(self):
        global CURRENTLY_CHARGING
        global KILL
        sleep(RAND_ID_SHOW_TIME)

        if not CURRENTLY_CHARGING:
            KILL = True
            GPIO.output(AUTHORISED_SIGNAL_PIN, GPIO.LOW)
            self.show_main_screen()

        self.currently_waiting = False
    """

    @route("/check_rand_id", methods=["GET"])
    def check_rand_id(self):
        global CUR_RAND_ID
        return jsonify({"rand_id": CUR_RAND_ID})

    @route("/authorise_charging", methods=["GET"])
    def authorise_charging(self):
        Thread(target=self.while_charging).start()

        return jsonify({"success" : True})

    @route("/reboot", methods=["GET"])
    def reboot(self):
        Thread(target=self._reboot).start()

        return jsonify({"success": True})

    @route("/version", methods=["GET"])
    def version(self):
        return jsonify({"version": 0.11})

    @route("/update", methods=["GET"])
    def update(self):
        system("git pull")
        Thread(target=self._reboot).start()

        return jsonify({"success": True})

    @route("/shutdown", methods=["GET"])
    def shutdown(self):
        Thread(target=self._shutdown).start()

        return jsonify({"success": True})

    def _reboot(self):
        sleep(0.1)
        system("sudo reboot")

    def _shutdown(self):
        sleep(0.1)
        system("sudo shutdown now")

    def heartbeat(self, charge_time):
        if DEBUG_SCALE_SECS_TO_MIN:
            charge_time *= 60

        charging_amount = CHARGE_POWER * (charge_time / 60 / 60)    # charge_time to hours

        requests.post(CHARGING_SERVER + "/heartbeat", json={"station_id": STATION_ID, "charging_amount": charging_amount})

    def while_charging(self):
        self.show_charging_screen(False)
        GPIO.output(AUTHORISED_SIGNAL_PIN, GPIO.HIGH)

        # Wait for charging to begin...
        while not GPIO.input(IS_CHARGING_PIN):
            sleep(0.5)

        self.show_charging_screen(True)
        charge_begin_time = int(time())
        heartbeat_time = charge_begin_time

        # Wait for charging to end...
        while GPIO.input(IS_CHARGING_PIN):
            if int(time()) > heartbeat_time + HEARTBEAT_TIME:
                self.heartbeat(int(time()) - charge_begin_time)
                heartbeat_time = int(time())

            sleep(0.5)

        charge_time = int(time()) - charge_begin_time
        GPIO.output(AUTHORISED_SIGNAL_PIN, GPIO.LOW)

        self.show_main_screen()

        if DEBUG_SCALE_SECS_TO_MIN:
            charge_time *= 60

        charging_amount = CHARGE_POWER * (charge_time / 60 / 60)    # charge_time to hours

        requests.post(CHARGING_SERVER + "/end_charging", json={"station_id": STATION_ID, "charging_amount": charging_amount})

if __name__ == "__main__":
    #ChargingStation()

    app = Flask(__name__)
    ChargingStation.register(app, route_base='/')
    app.run(host="0.0.0.0", port=5005)

    """
    bRunning = True
    while bRunning:
        for event in pygame.event.get():

            # Exit
            if event.type == pygame.QUIT:
                bRunning = False

            keys = pygame.key.get_pressed()

            if keys[pygame.K_ESCAPE]:
                bRunning = False
    """